package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	mautrix "maunium.net/go/mautrix"
	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"
	"os"
	_ "strconv"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func initDb(dbName string) (*sql.DB, error) {

	isNew := !fileExists(dbName)

	db, err := sql.Open("sqlite3", dbName)
	if err != nil {
		return db, err
	}

	if isNew {
		sqlStatement := `
                create table entries (content text unique primary key);
                `
		_, err := db.Exec(sqlStatement)
		if err != nil {
			db.Close()
			return db, err
		}
	}

	return db, err
}

type Configuration struct {
	DatabasePath  string
	ReactionEmoji string
	Username      string
	Homeserver    string
	AccessToken   string
	JoinMessage   string
}

func loadConfig() (Configuration, error) {
	// TODO: make properly XDG compliant
	// get command line argument flag or default to XDG_CONFIG_HOME compatible default path
	var config_path = flag.String("config", fmt.Sprintf("%s/.config/matrix-ranking-bot/config.json", os.Getenv("HOME")), "config file location")
	flag.Parse()

	// load config file and decode into struct
	file, _ := os.Open(*config_path)
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err := decoder.Decode(&configuration)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading config file at %s\n", *config_path)
	}

	return configuration, err
}

func main() {

	// load configuration settings from jsot file
	configuration, err := loadConfig()
	if err != nil {
		panic(err)
	}

	// initialize db and get handle
	db, err := initDb(configuration.DatabasePath)
	if err != nil {
		panic(err)
	}

	// store config options into local variables
	var emojiKey string = configuration.ReactionEmoji
	var username mid.UserID = mid.UserID(configuration.Username)
	var joinMessage string = configuration.JoinMessage

	// login to homeserver
	client, err := mautrix.NewClient(configuration.Homeserver, username, configuration.AccessToken)
	if err != nil {
		panic(err)
	}

	// this channel is for parsing reaction messages
	reactionChan := make(chan mevent.Event)

	// This thread parses reaction messages.
	// If they match the desired emojiKey,
	// the message is inserted into the database.
	go func() {
		defer db.Close()
		for {
			// recieve event to parse
			event := <-reactionChan

			// if the event is of the wrong type, ignore it
			if event.Type.Type != "m.reaction" {
				continue
			}
			// parse the event into the more detailed struct
			err = event.Content.ParseRaw(mevent.Type{"m.reaction", mevent.MessageEventType})
			if err != nil {
				panic(err)
			}

			// cast the event into ReactionEventContent
			reaction := event.Content.AsReaction()

			// check if the reaction is the user specified emoji, otherwise ignore
			if emojiKey != reaction.RelatesTo.Key {
				continue
			}

			// TODO: make logging optional with a command line flag
			// log new submission to terminal
			fmt.Printf("Inserting %s into db\n", reaction.RelatesTo.EventID.String())

			// database operations for insertion
			// begin database transaction
			tx, err := db.Begin()
			if err != nil {
				panic(err)
			}
			// prepare insertion statement
			stmt, err := tx.Prepare("INSERT or REPLACE into entries(content) values(?)")
			if err != nil {
				panic(err)
			}
			// execute statement with RelatesTo.EventID as the content
			// (insert new event into database)
			_, err = stmt.Exec(reaction.RelatesTo.EventID.String())
			if err != nil {
				panic(err)
			}
			// close the statement
			stmt.Close()
			// commit the database transaction
			tx.Commit()

			// send notice to channel confirming recipt of meme submission
			_, err = client.SendNotice(event.RoomID, fmt.Sprintf("Thanks %s, you meme submission has been recorded", event.Sender.String()))
			if err != nil {
				panic(err)
			}
		}
	}()

	// this channel is for submitting events to enable auto joining channels
	joinChan := make(chan mevent.Event, 10)

	go func() {
		for {
			event := <-joinChan
			// if the invite is not targeting us, ignore it
			if *event.StateKey != username.String() {
				continue
			}
			// if the event is of the wrong type, ignore it
			if event.Type.Type != "m.room.member" {
				continue
			}
			// parse the event into the more detailed struct
			err = event.Content.ParseRaw(mevent.Type{"m.room.member", mevent.StateEventType})
			if err != nil {
				panic(err)
			}

			// cast the event into MemberEventContent
			invite := event.Content.AsMember()

			// if the event is an invite, send the roomid to the joiner goroutine
			if invite.Membership == "invite" {
				fmt.Printf("You have been invited to %s\n", event.RoomID.String())
			} else {
				continue
			}

			fmt.Printf("Joining %s\n", event.RoomID.String())
			resp, err := client.JoinRoomByID(event.RoomID)
			if err != nil {
				fmt.Printf("Could not join channel %s\n", event.RoomID.String())
				fmt.Println(err)
			} else {
				fmt.Printf("Joined %s sucessfully\n", event.RoomID.String())
				fmt.Println(resp)
				// send message to channel confirming join
				_, err = client.SendNotice(event.RoomID, joinMessage)
				if err != nil {
					panic(err)
				}

			}
		}
	}()

	syncer := client.Syncer.(*mautrix.DefaultSyncer)

	// This callback captures m.room.member events, which is used to detect invites
	syncer.OnEventType(mevent.StateMember, func(eventSource mautrix.EventSource, event *mevent.Event) {
		joinChan <- *event
	})

	// This callback captures reaction messages for searching for memes
	syncer.OnEventType(mevent.EventReaction, func(eventSource mautrix.EventSource, event *mevent.Event) {
		reactionChan <- *event
	})

	initialResp, err := client.SyncRequest(30000, "", "", false, "")
	if err != nil {
		panic(err)
	}

	nextBatch := initialResp.NextBatch

	for {
		resSync, err := client.SyncRequest(30000, nextBatch, "", false, "")
		if err != nil {
			panic(err)
		}

		client.Store.SaveNextBatch(client.UserID, resSync.NextBatch)
		if err = client.Syncer.ProcessResponse(resSync, nextBatch); err != nil {
			panic(err)
		}

		nextBatch = resSync.NextBatch
	}

	// TODO: make sure it closes cleanly, though I think it's pretty clean right now
}
