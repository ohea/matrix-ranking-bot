module gitlab.com/ohea/matrix-ranking-bot

go 1.14

require (
	github.com/mattn/go-sqlite3 v1.14.0
	maunium.net/go/mautrix v0.5.1
)
